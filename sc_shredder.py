#!/usr/bin/env python3
################################################################################
##                                                                            ##
##  CIPHER FILE SHREDDER                                                      ##
##                                                                            ##
################################################################################

################################################################################
## LICENSE:                                                                   ##
################################################################################

"""
This software is licensed under the PolyForm Noncommercial License 1.0.0

In summary:
1. You may use, copy, share, or modify this code as long you respect its
noncommercial license.

2. You may use, copy, share, or modify this code as long you attribute it to its
original licensor by mentioning one of the 4-line Required Notices stated below.

3. You must indicate in comment if you have modified this code from its original
form by selecting the second modified Required Notice instead of the first one.

4. You can read the full license text here:
https://polyformproject.org/licenses/noncommercial/1.0.0

Include the following notice if you have not modified this code:

    Required Notice: Copyright @sN4Ke_c4Se (snake_case_software[at]pm.me | snakecase.software)
    Original source: https://gitlab.com/sN4Ke_c4Se/cipher_file_shredder
    Licensed under the PolyForm Noncommercial License 1.0.0
    (https://polyformproject.org/licenses/noncommercial/1.0.0)

If you have modified this code, include the following notice instead:

    Required Notice (modified): Copyright @sN4Ke_c4Se (snake_case_software[at]pm.me | snakecase.software)
    Modified from the original source: https://gitlab.com/sN4Ke_c4Se/cipher_file_shredder
    Licensed under the PolyForm Noncommercial License 1.0.0
    (https://polyformproject.org/licenses/noncommercial/1.0.0)

"""

################################################################################
## DESCRIPTION:                                                               ##
################################################################################

"""
Version: 0.1 (2023-10-09)

This script is intended to add an additional layer of security using encryption 
when deleting sensitive files. This script is still experimental, in testing 
phase, and is not recommended for any serious usage.

The script takes one or more files or directories in arguments, encrypts each 
file (or all files within a directory) a number of times, each time with a 
different key. The encryption key is only kept within the script's function and 
is never stored on file. The key variable is deleted from memory as soon as it 
is not needed anymore.

Each file, directory, or subdirectory is then renamed with a random set of 
characters. The file's metadata creation and modification dates are overwritten 
with generic ones. Lastly, the file is permanently deleted.

This operation aims to reduce the possibilities for data remanence after 
deletion. However, this is limited to the individual file and does not delete 
every possible traces of the file outside of this scope.

SECURITY WARNING: There are many limitations to keep in mind when deleting very 
sensitive data. For some types of drives and systems, individual file 
overwriting will still leave data remnants behind. Refer to the Limitations 
section from https://gitlab.com/sN4Ke_c4Se/cipher_file_shredder for more 
information on this. 

A trace that this script was used to encrypt and delete a file, as well as the 
date it was used, will likely remain on system and could be retrieved with more 
or less advanced tools (depending on systems and setups).

DISCLAIMER: The author of this script cannot be held responsible for any misuse 
of this script or any deletion accident. The user is solely responsible for any 
data loss. Use this script at your own risk.

"""


################################################################################
### ALL IMPORTS                                                              ###
################################################################################

import sys
sys.dont_write_bytecode = True ## <- Prevent Python from creating cache files
import os
import gc
import time 
import string
import shutil
import secrets
from os.path import exists
import cryptography ## <- Third-party https://pypi.org/project/cryptography/
from cryptography.fernet import Fernet


################################################################################
### ALL CONSTANTS                                                            ###
################################################################################

FILE_PY_MAIN = "sc_shredder.py"
ENCRYPTION_PASSES = 3

## PROGRAM OPTION VARIABLES: ###################################################
OP_HELP = ["-h", "--help"]
OP_SILENT = ["-s", "--silent"]
OP_DANGEROUS = ["--DANGEROUS"]
OP_VALID = OP_HELP + OP_SILENT + OP_DANGEROUS

## PROGRAM HEADER: #############################################################
MSG_HEADER = '''                                                  ___
                                               ➤-⎛- o⎞
    ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾    \\  ⎠
    sN4Ke_c4Se Cipher File Shredder               ⎠ /
    ⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎛ ⎛'''

## PROGRAM USAGE: ##############################################################
MSG_USAGE =f'''\
  \033[39;1;49mSECURITY WARNING:\033[0m Do not rely on this script to delete very sensitive data.

                    This script is still experimental and does not guarantee proper 
                    secure file erasure. Moreover, individual file crypto-shredders 
                    are not a completely reliable way to assure sensitive data has 
                    been properly and completely deleted in an unrecoverable way.

                    Depending on the type and usage of the drive where the data was 
                    stored, how long the data has been stored, its state of encryption, 
                    and many other factors, even after using a crypto-shredder there 
                    might still be ways to recover the data or metadata. Irrecoverable 
                    deletion cannot be completely assured by software means only.

  \033[39;1;49mUSAGE: python3 [path_to`{FILE_PY_MAIN}`] [file_to_delete] [optional: -option(s)]\033[0m

  USAGE EXAMPLE: python3 {FILE_PY_MAIN} example_file.txt --silent                         
  
  \033[39;1;49mOPTIONS AVAILABLE:\033[0m

  `{OP_HELP[0]}` or `{OP_HELP[1]}`: Displays the program usage and exit

  `{OP_SILENT[0]}` or `{OP_SILENT[1]}`: Displays only minimal deletion warning messages

  `{OP_DANGEROUS[0]}`: Deletes permanently all files and directories in arguments without any warning
                 \033[39;1;49mWARNING!\033[0m This option is obviously very dangerous! There is no undo!
'''     

## MESSAGE TYPES (with style): #################################################
STR_ERROR = "\n  \033[39;1;49mERROR!-> \033[0m" ## <- Bold
STR_NOTICE = "  \033[39;1;49mNOTICE-> \033[0m" ## <- Bold
STR_CONFIRM = "\n  \033[39;1;49mCONFIRMATION-> \033[0m" ## <- Bold
STR_INPUT = "\n  \033[39;1;49mREQUIRED USER CONFIRMATION: \033[0m\n  " ## <- Bold
STR_WARNING = "  \033[39;1;49mWARNING!-> \033[0m" ## <- Bold

## GENERAL MESSAGES: ###########################################################
MSG_EXIT = "  The program will now exit.\n"
MSG_NO_DELETION = STR_NOTICE + "No files were deleted."
MSG_ENCRYPTING_F = STR_NOTICE + "Ecrypting file for cryptographic shredding: '{}'"
MSG_PASS_COUNT_F = "  Pass {}..."
MSG_RENAMING = STR_NOTICE + "Renamed file to delete with random name."
MSG_DELETING = STR_NOTICE + "Deleting encrypted file permanently..."
MSG_DELETING_DIR = STR_NOTICE + "Deleting encrypted directory permanently..."
MSG_DELETED_FILE_F = STR_CONFIRM + "Deletion confirmed for: '{}'"
MSG_DELETION_CONFIRMED = \
"\n  \033[39;1;49mDELETION COMPLETED:\033[0m Cryptographic files erasure completed."

## WARNING_ MESSAGES: ##########################################################
WARN_SELECTION_TO_DELETE = STR_WARNING + \
"You have selected the following files and/or directories,\n" \
+ "  including all files and subdirectories inside of it, to be permanently deleted:\n"

WARN_SELECTION_CONFIRM_IN = STR_INPUT + \
"Do you want to delete permanently all of the above listed files and/or directories?\n" \
+ "  Type 'DELETE' to continue, anything else to exit: "

WARN_DELETION_AGAIN = \
"\n  \033[39;1;49mDANGEROUS! This operation cannot be undone!\033[0m\n" \
+ "  Are you certain you want to \033[39;1;49mdelete permanently\033[0m all of the listed files?\n"

WARN_DELETION_AGAIN_IN = \
"  Type 'DELETE' to delete permanently all files and directories recursively, anything else to quit: "

## ERROR MESSAGES: #############################################################
ERR_NO_VALID_FILE = STR_ERROR + \
"No valid file(s) was received in arguments.\n" \
+ "  Restart the program with at least one valid file to delete (full path to file).\n" \
+ "  Run the program with the option '{}' or '{}' to display the program usage.".format(OP_HELP[0], OP_HELP[1])

ERR_SIZE_NOT_CALCULATED_F = STR_ERROR + \
"The size for '{}' could not be calculated. Displayed size might be inaccurate."

ERR_READING_FILE_F = STR_ERROR + \
"An error occured while opening the file: '{}'\n" \
+ "  The file was not properly deleted. Try running the program again."

ERR_WRITING_FILE_F = STR_ERROR + \
"An error occured while overwriting the file: '{}'\n" \
+ "  The file was not properly deleted. Try running the program again."

ERR_RENAMING_FILE_F = STR_ERROR + \
"An error occured while renaming the file: '{}'\n" \
+ "  The file was not properly renamed and could not be properly deleted."

ERR_DELETING_FILE_F = STR_ERROR + \
"An error occured while deleting the file: '{}'\n" \
+ "  The encrypted file renamed '{}' was not properly deleted.\n"

ERR_DELETING_DIR_F = STR_ERROR + \
"An error occured while deleting the directory: '{}'\n" \
+ "  The encrypted directory renamed '{}' was not properly deleted.\n"

ERR_SCANNING_DIR_F = STR_ERROR + \
"An error occured while scanning the directory to encrypt it with the file: '{}'\n" \
+ "  The directory was not properly encrypted and could not be properly deleted.\n"

ERR_FILE_TYPE_F = STR_ERROR + \
"An error occured while encrypting and deleting the file or directory: '{}'"

ERR_OVERWRITING_METADATA_F = STR_ERROR + \
"An error occured while overwriting the file time metadata: '{}'\n" \
+ "  The file was not properly deleted. Try running the program again."

################################################################################
### ALL FUNCTIONS                                                            ###
################################################################################

def delete_file_or_directory_permanently(file, new_file_name):
    """Delete a file or a directory and all its content (recursively) 
    permanently."""
        
    if os.path.isfile(new_file_name):
        print_verbose_messages(MSG_DELETING)
        try:
            os.unlink(new_file_name)
        except:
            print_verbose_messages(ERR_DELETING_FILE_F.format(file, new_file_name))
    elif os.path.isdir(new_file_name):
        print_verbose_messages(MSG_DELETING_DIR)
        try:
            shutil.rmtree(new_file_name, ignore_errors=False)
        except:
            print_verbose_messages(ERR_DELETING_DIR_F.format(dir, new_file_name))
    
def encrypt_directory_to_delete_recursively(dir):
    """Encrypt all files within the directory to delete recursively using the 
    Fernet recipe from the Cryptography library, which uses AES 128-bit 
    symmetric-key encryption. Repeat the encryption process the number of times
    according to set passes. Flush the encryption key and Fernet object from 
    memory. Rename all subdirectories with random names. Overwrite all files
    creation and modification dates."""

    ## Collect all files and subdirectories within the directory:
    directories_in_dir = []
    files_in_dir =[]
    for path, sub_dirs, files in os.walk(dir):
        try:
            for sub_dir in sub_dirs:
                dir_path = os.path.join(path, sub_dir)
                directories_in_dir.append(dir_path)
            for file in files:
                file_path = os.path.join(path, file)  
                files_in_dir.append(file_path)
        except:
            print_verbose_messages(ERR_SCANNING_DIR_F.format(file_path))
            sys.exit()
    ## Encrypt, overwrite, and rename all files within the (sub)directory:
    for file in files_in_dir:
        print_verbose_messages("")
        encrypt_file_to_delete(file)
        overwrite_file_time_metadata(file)
        rename_file_to_delete(file)       
    ## Rename all subdirectories and overwrite time metadata:
    directories_in_dir = sorted(directories_in_dir, key = lambda x: x.count('/'))
    directories_in_dir = list(reversed(directories_in_dir))
    for subdir in directories_in_dir:
        new_subdir = rename_file_to_delete(subdir)
        overwrite_file_time_metadata(new_subdir)
    ## Clear variables from memory:
    del(directories_in_dir)
    del(files_in_dir)
    gc.collect()
    
def encrypt_file_to_delete(file):
    """Encrypt a file to delete using the Fernet recipe from the Cryptography
    library, which uses AES 128-bit symmetric-key encryption. Repeat the 
    encryption process the number of times according to set passes. Flush the
    encryption key and Fernet object from memory."""

    print_verbose_messages(MSG_ENCRYPTING_F.format(file))
    for p in range(ENCRYPTION_PASSES):
        ## Print message for each pass if not silent:
        if not silent:
            sys.stdout.write(MSG_PASS_COUNT_F.format(str(p+1)))
        sys.stdout.flush()
        ## Generate temporary key (not saved anywhere else):
        key = Fernet.generate_key()
        fern = Fernet(key)
        ## Read file content and encrypt it:
        try:
            with open(file, "rb") as f:
                file_content = f.read()
            token = fern.encrypt(file_content)
        except IOError:
            print_verbose_messages(ERR_READING_FILE_F.format(file), MSG_EXIT)
            sys.exit()
        ## Overwrite file content with encryption:
        try:
            with open(file, "wb") as f:
                f.write(token)
        except IOError:
            print_verbose_messages(ERR_WRITING_FILE_F.format(file), MSG_EXIT)
            sys.exit()
        ## Delete variables key and fern to reduce risk of data remanance:
        del(key)
        del(fern)
        gc.collect()        
    print_verbose_messages("")

def get_directory_size(dir):
    """Return a formatted string with a human readable directory size."""

    dir_size = 0
    for path, sub_dirs, files in os.walk(dir):
        for file in files:
            file_path = os.path.join(path, file)
            try:
                file_size = os.path.getsize(file_path)
                dir_size += file_size
            except FileNotFoundError:
                print_verbose_messages(ERR_SIZE_NOT_CALCULATED_F.format(file_path))
                continue
    for unit in ['BYTES', 'KB', 'MB', 'GB', 'TB']:
        if dir_size < 1024.0:
            break
        dir_size = dir_size / 1024.0
    dir_size = f"{dir_size:.1f}"
    return f"{dir_size:>5} {unit}"

def get_file_size(file):
    """Return a formatted string with a human readable file size."""

    file_size = os.path.getsize(file)
    for unit in ['BYTES', 'KB', 'MB', 'GB', 'TB']:
        if file_size < 1024.0:
            break
        file_size = file_size / 1024.0
    file_size = f"{file_size:.1f}"
    return f"{file_size:>5} {unit}"

def overwrite_file_time_metadata(file):
    """Overwrite a file's or directory's creation and modification dates with 
    generic ones."""

    try:
        os.utime(file, (1, 1))
    except:
        print_verbose_messages(ERR_OVERWRITING_METADATA_F.format(file), MSG_EXIT)

def print_deletion_warning(valid_files):
    """Return `True` if user confirms the selected file(s) deletion."""

    directories = [dir for dir in valid_files if os.path.isdir(dir)]
    files = [file for file in valid_files if os.path.isfile(file)]
    print_verbose_messages(WARN_SELECTION_TO_DELETE)
    ## Print warning and list all directories:
    if directories:
        print_verbose_messages("    \033[39;1;49mDIRECTORIES TO DELETE RECURSIVELY:\033[0m")
        for dir in directories:
            dir_size = get_directory_size(dir)
            modified_time = os.path.getmtime(dir)
            last_modified = time.ctime(modified_time)
            if silent:
                print(f"  {dir}")
            else:
                print(f"    {dir}\t| size: {dir_size} \t| last modified: {last_modified}")
        print_verbose_messages("")
    ## Print warning and list all files:
    if files:
        print_verbose_messages("    \033[39;1;49mFILES TO DELETE:\033[0m")
        for file in files:
            file_size = get_file_size(file)
            modified_time = os.path.getmtime(file)
            last_modified = time.ctime(modified_time)
            if silent:
                print(f"  {file}")
            else:
                print(f"    {file}\t| size: {file_size} \t| last modified: {last_modified}")
        print_verbose_messages("")
    ## Ask user input to confirm permanent deletion:
    confirm = input(WARN_SELECTION_CONFIRM_IN)
    if confirm == 'DELETE':
        return True
    return False

def print_deletion_warning_again(valid_files):
    """Return `True` if user confirms the selected file(s) deletion."""

    print(WARN_DELETION_AGAIN)
    for file in valid_files:
        print(f"    {file}")
    print()
    ## Ask user input to confirm permanent deletion:
    confirm = input(WARN_DELETION_AGAIN_IN)
    if confirm == 'DELETE':
        print()
        return True     
    return False

def print_program_usage_and_exit():
    """Print the program usage and exit program."""

    print()
    print(MSG_USAGE)
    sys.exit()

def print_verbose_messages(*messages):
    """Print all messages received if Silent Mode was not selected."""

    if silent == False:
        for message in messages:
            print(message)

def rename_file_to_delete(file):
    """Rename the file or directory to be deleted with a new random file name 
    and Return it with the full path."""

    if file[-1:] == "/":
        file = file[:-1]
    file_path = file.split("/")
    file_name = file_path[-1]
    file_path = "/".join(file_path[:-1]) + "/"
    new_file_name = "".join([secrets.choice(string.hexdigits) for c in range(24)])
    new_file_path = file_path + new_file_name
    try:
        print_verbose_messages(MSG_RENAMING)
        os.rename(file, new_file_path)
    except:
        print_verbose_messages(ERR_RENAMING_FILE_F.format(file), MSG_EXIT)
        sys.exit()
    return new_file_path

def validate_arguments(args):
    """Return a list of valid files and directories received in arguments."""

    args = sys.argv[1:]
    ## Remove valid options from args:
    args = [arg for arg in args if arg not in OP_VALID]
    ## Return all file(s) that exist:
    valid_files = []
    for arg in args:
        if exists(arg):
            valid_files.append(arg)
    if not valid_files:
        print_verbose_messages(ERR_NO_VALID_FILE, MSG_EXIT)
        sys.exit()
    return valid_files

def validate_options(args):
    """Return a list of valid program options, return an empty list if none.
    Return variable help, silent, and dangerous, set to `True` if in options 
    list."""

    options = []
    help =  False
    silent = False
    dangerous = False
    for arg in args:
        if arg in OP_VALID:
            options.append(arg)
    if OP_HELP[0] in options or OP_HELP[1] in options:
        help = True
    if OP_SILENT[0] in options or OP_SILENT[1] in options:
        silent = True
    if OP_DANGEROUS[0] in options:
        dangerous = True
    return options, help, silent, dangerous

def verify_file_deletion(file, new_file_name):
    """Return `True` if old or new file name not found and print a confirmation 
    message. Return `False if old or new file name is found."""

    if exists(new_file_name) or exists(file):
        print_verbose_messages(ERR_DELETING_FILE_F.format(file, new_file_name))
        return False
    else:
        print_verbose_messages(MSG_DELETED_FILE_F.format(file))
        return True


## PROGRAM MAIN: ###############################################################
def main():
    """
    Validate options and files received in arguments. Print 2 layers of warning
    messages requiring user confirmation before proceeding with deletion, unless
    the Dangerous option is selected. For each valid file or directory received
    in argument: encrypt (AES 128-bit symmetric-key), rename randomly, overwrite
    time metadata (creation and modification dates), then delete file or
    directory recursively. Verify deletion. Clean the script's memory. And 
    finally, print a success message if all deletions were successful. 
    """
    
    ## Validate option(s) and argument(s), display help it selected:
    global silent
    options, help, silent, dangerous = validate_options(sys.argv)
    if help:
        print_program_usage_and_exit()
    print_verbose_messages("")
    valid_files = validate_arguments(sys.argv)

    ## Print warning messages and ask user confirmation (twice):
    if not dangerous:
        confirmation = print_deletion_warning(valid_files)
        if not confirmation:
            print_verbose_messages(MSG_NO_DELETION, MSG_EXIT)
            sys.exit()
        second_confirmation = print_deletion_warning_again(valid_files)
        if not second_confirmation:
            print_verbose_messages(MSG_NO_DELETION, MSG_EXIT)
            sys.exit()
    
    ## If deletion confirmed or Dangerous option selected, start deletion:
    print_verbose_messages("  DELETION IN PROCESS...")
    new_file_name = ""
    deleted = []
    for file in valid_files:
        ## 1. Encrypt file or directory (recursively):
        if os.path.isfile(file):
            encrypt_file_to_delete(file)
        elif os.path.isdir(file):
            encrypt_directory_to_delete_recursively(file)
        ## 2. Rename file or directory:
        new_file_name = rename_file_to_delete(file)
        ## 3. Overwrite time metadata:
        overwrite_file_time_metadata(new_file_name)
        ## 4. Delete file or directory permanently (recursively):
        delete_file_or_directory_permanently(file, new_file_name)
        ## 5. Verify deletion:
        deleted.append(verify_file_deletion(file, new_file_name))

    ## Clean variables from memory:
    del(file)
    del(valid_files)
    del(new_file_name)
    del(options)
    gc.collect() 

    ## Program confirmation:
    if deleted and all(deleted):
        print_verbose_messages(MSG_DELETION_CONFIRMED)
    print_verbose_messages("")

################################################################################

if __name__ == "__main__":
    main()
