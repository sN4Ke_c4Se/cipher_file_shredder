# sN4Ke_c4Se Cipher File Shredder 🔥

```
                                       ___
                                    ➤-⎛- o⎞
  ‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾‾   \  ⎠
   sN4Ke_c4Se Cipher File Shredder     ⎠ /
  ⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎽⎛ ⎛
  

```

## About

This script is intended to add an additional layer of security using encryption when deleting sensitive files. This script is still experimental, in testing phase, and is not recommended for any serious usage.

The script takes one or more files or directories in arguments, encrypts each file (or all files within a directory) a number of times, each time with a different key. The encryption key is only kept within the script's function and is never stored on file. The key variable is deleted from memory as soon as it is not needed anymore.

Each file, directory, or subdirectory is then renamed with a random set of characters. The file's metadata creation and modification dates are overwritten with generic ones. Lastly, the file is permanently deleted.

This operation aims to reduce the possibilities for data remanence after deletion. However, this is limited to the individual file and does not delete every possible traces of the file outside of this scope. Read the 'Security Warning' and 'Additional Limitations and Warnings' sections below for more on this.

## License 

You can use this script for noncommercial usage only, with attribution, following its [PolyForm Noncommercial License 1.0.0](https://polyformproject.org/licenses/noncommercial/1.0.0/)

## Requirements

This script uses the external `cryptography` library: [https://pypi.org/project/cryptography/](https://pypi.org/project/cryptography/)

## Version 0.1

This script is still in experimental phase and is untested.

## Disclaimer

The author of this script cannot be held responsible for any misuse of this script or any deletion accident. The user is solely responsible for any data loss. Use this script at your own risk.

## Security Warning

**Do not rely on this script to delete very sensitive data.** 

This script is still experimental and does not guarantee proper secure file erasure. Moreover, individual file crypto-shredders are not a completely reliable way to assure sensitive data has been properly and completely deleted in an unrecoverable way. 

Depending on the type and usage of the drive where the data was stored, how long the data has been stored, its state of encryption, and many other factors, even after using a crypto-shredder there might still be ways to recover the data. Irrecoverable deletion cannot be completely assured by software means only. 

There are many limitations to keep in mind when deleting sensitive data using this script. For some types of drives and systems, individual file overwriting will still leave data remnants behind. Read the 'Additional Limitations and Warnings' section below for more information on this.

You can learn more about the benefits and limitations of crypto-shredding here: [https://en.wikipedia.org/wiki/Crypto-shredding](https://en.wikipedia.org/wiki/Crypto-shredding)

## Additional Limitations and Warnings

1. Individual file shredding is limited in reliability. Full disk encryption from the start and full disk overwrite for deletion is always preferable whenever possible.

2. This script is not secured to be used remotely and should never be used remotely.

3. Data remanence, possibly even a full unencrypted previous version of the file, is likely to remain (hidden) on drive for certain types of solid-state drives (SSD) and USB drives, and could be retrieved with more or less advanced tools (depending on the drive and system types).

4. Metadata might still be recoverable with advanced techniques for some files.

5. Traces of the erasure processes using this script are likely to remain in memory and possibly on drive as well, and could be retrieved with more or less advanced tools (depending on systems and setups).

6. Although files to delete are renamed before deletion, traces of the script's command could remain in system memory and/or bash history including the deleted file's initial name (ex: in the `.bash_history` file).

7. It is recommended to make sure the file to delete isn't in use during deletion and to turn off the computer after running this script to reduce the risk of data remanence in memory.

8. Do not use the `--DANGEROUS` option without being extremely cautious. With this option enabled, there will be no warning, no confirmation, and no possibility to undo this action after permanent deletion.

9. This script is not optimized to delete very large files and will likely be slow.

## Encryption

Encryption prior to deletion is done using the Fernet recipe from the [Cryptography library](https://pypi.org/project/cryptography/), which uses AES 128-bit symmetric-key encryption. Full encryption of each file is repeated a set number of times (3 times in this version). The encryption key is only kept in a script variable and is not stored anywhere outside of the function. Once a file has been encrypted a number of times, the key variable (as well as the Fernet object) is deleted from the script's memory.

## Installation

`git clone git@gitlab.com:sN4Ke_c4Se/cipher_file_shredder.git`

##### With Python Virtual Environment:

`cd [path to directory: cipher_file_shredder]`

`python3 -m venv venv`

`source venv/bin/activate`

`pip install -R requirements.txt`

> NOTE: Do not forget to activate your virtual environment each time you run the script, or install the cryptography library globally.

## Usage

1. Use this program carefully. Once the file is deleted, there is no way to undo this action.

2. The script can take multiple files and directories to be deleted in arguments.

3. All directories will be encrypted and deleted recursively (including all its content).

4. Deleted files and directories are not sent to the trash, they are deleted completely and permanently as soon as the command is executed.

## Command-Line Usage & Options

##### In a terminal: 

`python3 [path_to_the_file: sc_.py] [file to delete] [optional: more files to delete] [optional: -option]`

##### Usage example: 

`python3 sc_shredder.py example_file.txt --silent`

##### Options available: 

`-h` or `--help`: Displays the program usage and exit

`-s` or `--silent`: Displays only minimal deletion warning messages

`--DANGEROUS`: Deletes permanently all files and directories in arguments without any warning
               
> **WARNING!** This option is obviously very dangerous! There is no undo!



